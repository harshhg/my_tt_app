from django.contrib import admin
from .models import Players
# Register your models here.
class PlayersAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = (
        'name','email','gender','age')

admin.site.register(Players,PlayersAdmin)