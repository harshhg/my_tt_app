from django.db import models
# from  my_tt_app.login.models import Account

# Create your models here.

class Tournaments(models.Model):
    name = models.CharField(max_length=100, default=None, null=False)
    start_date = models.DateField(default=None, null=False)
    end_date = models.DateField(default=None, null=False)
    max_score = models.IntegerField(default=None, null=False)
    is_match_created = models.BooleanField(default=False, null=True)
    is_completed = models.BooleanField(default=False)
    winner_id = models.IntegerField(verbose_name='winner',default=None, null=True)
    # status = models.CharField(max_length=10, default='ongoing', null=False)
    total_players = models.IntegerField(default=None, null=False)
    user = models.ForeignKey('login.Account', on_delete=models.DO_NOTHING, blank=True)
    tournament_image = models.ImageField(upload_to='media/tournaments/', default=False, null=True)
    players = models.ManyToManyField('players.Players', related_name='player')

# class TournamentsWinner(models.Model):
#     player_id = models.IntegerField(max_length=10, default=None, null=False)
#     tournament = models.OneToOneField(Tournaments, on_delete=models.DO_NOTHING, blank=True)



