from rest_framework import serializers
from .models import Account

class RegistrationSerializer(serializers.ModelSerializer):
    # password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    class Meta:
        model = Account
        fields = ['first_name', 'last_name', 'email', 'password']
        # extra_kwargs = {
        #     'password': {'write_only': True}
        # }
    def save(self):
        account = Account(
            first_name = self.validated_data['first_name'],
            last_name = self.validated_data['last_name'],
            email=self.validated_data['email'],
        )
        password = self.validated_data['password']
    #     # password2 = self.validated_data['password2']
    #     #
    #     # if password != password2:
    #     #     raise serializers.ValidationError({'password': 'Password must match'})
        account.set_password(password)
        account.save()
        return account

class UserSerializer(serializers.ModelSerializer):
    profile_imageurl = serializers.SerializerMethodField()
    class Meta:
        model = Account
        fields = ['id', 'first_name', 'last_name', 'email','profile_imageurl','age','gender']
    def get_profile_imageurl(self,account):
        request = self.context.get('request')
        if request:
            profile_imageurl = account.profile_image.url
            return request.build_absolute_uri(profile_imageurl)



class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=300, required=True)
    password = serializers.CharField(required=True, write_only=True)

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'first_name', 'last_name', 'profile_image','age','gender']